import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from time import time
import requests
from datetime import datetime
import json


def init_firebase():
  # Use a service account
  if not len(firebase_admin._apps):
    cred = credentials.Certificate({
      "type": "service_account",
      "project_id": "findhome-217708",
      "private_key_id": "ab04be04d4040a9b3863d62ad26f8b75c67b302f",
      "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDSv+XtWnhJvyvZ\nnsLV6nQRfuMlwh5YFXCd+3yVze3+JldJTjO/FO5r4+LteSStXhX5szDvytSc27Go\nlemhOCIzJ7DqPoBM3QH58ti/l/T2DA4rE7qVz1YioQSPoQECbtOTojw9emmk2l2C\n6R9VujTdeEjbaZ7GwXm2iul846i5s/qHTpOT1HLd4htROvVWvG3d2JSx7G9D7DvB\nQnQIrr03lKXFHUoin1mHu0X4mJCc9EbMpvzc0eLlcVA3GIvSg0Zw66JI8oKUmfL3\ng+U/ZmI4MUoBRvXiAWQONifKZTY8lmykhlIQmNZEaOeurWqoJauLvpWxnpT7qFk6\nXGrWZHNVAgMBAAECggEAB4A/gi9sZRmPXryXwYPe/jQWiG9B47oy9YMwMmfE3v0a\nhw19aiwSoVdC1NdxOVTGMkqRBRPidd5xkrxv4eZXfJvJdZ45tE1QcZxAX9oGz2QV\n9AK3gHdY9jy7Zr5Lr3xmKOocZpwZsFL6WDtiZgt4MA1vhb8FtkbIGs8Tvjd0d6mH\nmKZLM56PyErxbCwmRDJnSjVx6P6bGHYIzbVLNPjFTHK1Tc5sASoLuZ4igKPX0w8A\nRlNSMAJIvM7tWovfuBI0O28UPWe6HM4UsAQIcEwunEFKElNz9CbHUv8rQwUTKKUn\n+ibBI2EmhkQG7UuKyPDQG9judFI9oQ4MyTPThv+sqQKBgQD103zOsJ+B0EqdN1GR\nlto/1byG+4frkRygG4VbRWKmggmQdwbYbhg8Pwu2ouvhzvbyHE2CdTsEihjuV2Ko\nmz1+PvwfUfSRSrOF8rekX0NrU5/QEmJs+PVjuiksqgaowBhCQQqMhWr4V4jbrTRB\nbLm1fn9BQ6Aj32hpcx28UEVXOQKBgQDbeMbt6nxDHbNA/p8yTlHepvJA3MaaNUVJ\nDXNRWKHbEqWRuQzoVcM96p5MOFe2j9qYV1BXDnd2WY2Jx6Z+Vawp0gkCYGtQr1n7\n2zlsMxEKJHsf7QY3LAqyM7+doGRQL3EHJxYvWIjedK9CpzLSM92eow0WFRyn5dcN\nQ5sxlYdA/QKBgEyhxxBvwumm0vu7mPP3wMWRjZnOo4ptzCylJ6dSM/SQKQcd88ZY\nW7m8NZyToVDdQeIrrIX0oZdCrY7vMtQ4wKywWcwxsjnwvZIOHe1hvRSiFvm4IXKz\nEVwrVlaUEktcxrMwr2cq2DPepc1FazrR9/p6GpVvzWYhaN0mg6iGpXcRAoGAVdqQ\nTx+sToD+z00Lr50tr6C1IaR72HVATkyBsO+wGUfZFwFByF1MTBtMo1pjDZqGk5OM\nbRHOr7BmiuBcBA21lIKCXbikImMGG1ztwKLVZpdQxvNsvAZxhLZzndDRjQCSAY9L\nwWer2wHgUX2ghtQdPOJhyyKlHBTwJFMvEZ2B1G0CgYAtXf9KcFAvSnge+3FFigp6\nyGnClB9TrG0qhmXZPLLYkKkDAACitSQXlZ4EQ5PW3Xr2EKlC/OgEueyPMm5nz+R/\n4CEf4AFGeX7efetfOq65y7ro7FIFbsP7eY7YUZOKFuifGfAWKvoCGmMkJR0nPqfo\ntH2JIdwkWcXBGPRuPTGoPg==\n-----END PRIVATE KEY-----\n",
      "client_email": "frank-00@findhome-217708.iam.gserviceaccount.com",
      "client_id": "106666234366259868875",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/frank-00%40findhome-217708.iam.gserviceaccount.com"
    })
    firebase_admin.initialize_app(cred)


def set_data(collection_name, document_name, data_in_dict):
  # To create or overwrite a single document, use the set() method:
  init_firebase()
  db = firestore.client()
  db.collection(collection_name).document(document_name).set(data_in_dict)


def midland_18_district():
  # source 'https://www.midland.com.hk/property-price-chart/'
  url = 'https://wws.midland.com.hk/mpp/data/district_pricechange?lang=en'
  req = requests.get(url).text
  d = json.loads(req)['district']
  for doc in d.values():
    doc.update({'_create': datetime.now()})
    print(doc)
    set_data('reference_price', doc['dist_id'], doc)

  url2 = 'https://wws.midland.com.hk/mpp/data/region_avg_price'
  req = requests.get(url2).text
  d = json.loads(req)['mr_index']['mr_index_1']
  d.update({'_create': datetime.now()})
  print(d)
  set_data('reference_price', 'mr_index', d)


def job(request):
  start = time()
  midland_18_district()
  return str(time() - start)


# midland_18_district()
