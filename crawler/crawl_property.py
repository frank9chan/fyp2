from requests import get, post
from bs4 import BeautifulSoup as bs
# import pandas as pd
import json
import re
from time import sleep
from firebase_func import add_data, get_data, get_system_data_is_rentable

__testing = False


def ricacorp():
    # src = https://property.ricacorp.com/post;type=r;language=EN;page=1;orderBy=overallDateModified%20desc
    for lang in ['EN', 'HK']:   # the language of the crawled data
        if __testing:
            i = 286
        else:
            i = 0

        while True:
            i = i + 1
            url = 'https://property.ricacorp.com/rcAPI/rcPost?page={}&offset={}&' \
                  'language={}&orderBy=overallDateModified%20desc&agreementType=5'.format(i, (i-1)*10, lang)
            req = get(url).text
            d = json.loads(req)
            try:
                for dd in d['results']:
                    dd.update({'is_rentable': get_system_data_is_rentable()})
                # print(d['results'][0])
                # print('-------------')
                # print(d['results'][1])
                # print('page =', i)
                sleep(0.1)
                add_data('historical_price', d['results'])
                # insert d['results']

            except IndexError:
                print('-- the end of ricacorp() --', lang)
                break


def centanet():
    def lang_hk():
        # chinese only
        # src = http://hk.centanet.com/findproperty/zh-HK/Home/SearchResult/?minarea=&maxarea=&areatype=N&posttype=R&src=C
        if __testing:
            _page = 751
        else:
            _page = 0

        while True:
            _page = _page + 1
            url = 'http://hk.centanet.com/findproperty/BLL/Result_SearchHandler.ashx?url=http%3A%2F%2Fhk.centanet.com%2Ffindproperty%2Fzh-HK%2FHome%2FSearchResult%2F%3Fareatype%3DN%26posttype%3DR%26src%3DC%26sortcolumn%3Dprice%26sorttype%3Dasc%26limit%3D-1%26currentpage%3D' + str(_page)
            req = get(url).text

            # inside json.loads(req): agentList post summary agentType
            d = json.loads(req)['post']
            d = bs(d, 'lxml').find_all("div", {"class": "SearchResult_Row"})

            result = []
            for i in d:
                if i.text:
                    the_post = re.sub(r'\s+', ' ', str(i))
                    the_post_soup = bs(the_post, 'lxml')
                    _postid = the_post_soup.find('div').attrs['postid']
                    _region = the_post_soup.find_all('p')[2].text.strip()
                    _header = the_post_soup.find('span', {"class": "ContentInfo_Header"}).text.strip()
                    _real_size = the_post_soup.find('p', {"class": "ContentInfo_SizeStr fLeft"}).text.rstrip('-').strip()\
                        .split(':')[1].replace('呎', '')
                    _rent_price = the_post_soup.find_all('span', {"class": "LargeString"})[-1].text.rstrip('-').strip()\
                        .replace('$', '').replace(',', '')
                    _img = the_post_soup.find('img').attrs['src']
                    try:
                        _rooms = the_post_soup.find('p', {"class": "ContentInfo_DetailStr_Lf fLeft OneRow"}).text.strip()\
                            .replace('房', '')
                    except AttributeError:
                        _rooms = None
                    try:
                        _age = the_post_soup.find('p', {"class": "ContentInfo_DetailStr_Lf leftBorder fLeft OneRow"})\
                            .text.strip().replace('年樓齡', '')
                    except AttributeError:
                        _age = None

                    result.append({'_postid': _postid, '_img': _img, '_region': _region, '_header': _header,
                                   '_real_size': _real_size, '_rent_price': _rent_price, '_rooms': _rooms, '_age': _age})
            if not result:
                return print('-- end of centanet() --')
            print(result)
            print('_page =', _page)
            sleep(0.5)
            # insert result
    lang_hk()


def squarefoot(page=1):
    # https://www.squarefoot.com.hk/en/rent/list/?sortBy=posted-desc&page=1
    print('current page = ', page)
    headers = {'content-type': 'application/json'}
    data = '{"search":{"channel":"rent","suggestions":[],"filters":{"sortBy":"posted-desc"},"pageNumber":"'+str(page)+'"}}'
    response = json.loads(post('https://www.squarefoot.com.hk/consumer/api/search', headers=headers, data=data).text)
    print(response['items'])        # <------------
    # if response["nextPageToken"]:
    #     return squarefoot(response["nextPageToken"])
    # else:
    #     return print('-- end of squarefoot()')
    for i in response['items']:
        add_data('new_src', i)

# def midland():
#     # https://en.midland.com.hk/find-property/#list


if __name__ == '__main__':
    # truncate the table before insert to secure all data can be rented
    ricacorp()
    # centanet()
    # squarefoot()
