import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

path_to_firebase_json = 'C:/Users/a/.ssh/frank-findhome.json'


# https://firebase.google.com/docs/firestore/manage-data/add-data?authuser=0
# https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/firestore/cloud-client/snippets.py

def init_firebase():
    # Use a service account
    if not len(firebase_admin._apps):
        cred = credentials.Certificate(path_to_firebase_json)
        firebase_admin.initialize_app(cred)


def get_data(collection_name):
    init_firebase()
    db = firestore.client()

    results = db.collection(collection_name).stream()

    for result in results:
        print(result.to_dict())


def get_system_data_is_rentable():
    init_firebase()
    db = firestore.client()

    return db.collection('system_setting').document('is_rentable').get().to_dict()['is_rentable']


def add_data(collection_name, data_in_dict):
    init_firebase()
    db = firestore.client()

    db.collection(collection_name).add(data_in_dict)


def set_data(collection_name, document_name, data_in_dict):
    # To create or overwrite a single document, use the set() method:
    init_firebase()
    db = firestore.client()
    db.collection(collection_name).document(document_name).set(data_in_dict)


def delete_data(collection_name, field_name, field_value):
    init_firebase()
    db = firestore.client()

    def del_limit(collection_name, field_name, field_value, batch_size=10):
        deleted = 0
        docs = db.collection(collection_name).where(field_name, u'==', field_value).limit(batch_size).stream()
        for doc in docs:
            print('--deleting:', doc.to_dict())
            doc.reference.delete()
            deleted = deleted + 1
        print('end del limit')
        if deleted >= batch_size:
            return del_limit(collection_name, field_name, field_value, batch_size)

    del_limit(collection_name, field_name, field_value, batch_size=10)


# def delete_full_collection():
#   db = firestore.Client()
#
#   # [START delete_full_collection]
#   def delete_collection(coll_ref, batch_size):
#     docs = coll_ref.limit(10).get()
#     deleted = 0
#
#     for doc in docs:
#       print(u'Deleting doc {} => {}'.format(doc.id, doc.to_dict()))
#       doc.reference.delete()
#       deleted = deleted + 1
#
#     if deleted >= batch_size:
#       return delete_collection(coll_ref, batch_size)
#
#   # [END delete_full_collection]
#
#   delete_collection(db.collection(u'cities'), 10)


if __name__ == '__main__':
    # get_data('reference_price')
    add_data('d', {'q': '1', 'w': '3'})
    add_data('d', {'q': '1', 'w': '4'})
    add_data('d', {'q': '2', 'w': '52'})
    add_data('d', {'q': '1', 'w': '2e'})
    add_data('d', {'q': '1', 'w': '223'})
    add_data('d', {'q': '2', 'w': '2e'})
    add_data('d', {'q': '1', 'w': '2w'})
    add_data('d', {'q': '1', 'w': 'w2'})
    add_data('d', {'q': '2', 'w': '22'})
    add_data('d', {'q': '1', 'w': 'd2'})
    add_data('d', {'q': '1', 'w': '2s'})
    add_data('d', {'q': '2', 'w': 'e'})
    # set_data('system_setting', 'is_rentable', {'is_rentable': 1})
    print('=======================')

    delete_data('d', 'q', '1')
    print('=======================')
    get_data('d')

    # print(get_system_data_is_rentable())
