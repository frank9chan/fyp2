import requests

headers = {'content-type': 'application/json'}
data = '{"search":{"channel":"rent","suggestions":[],"filters":{"sortBy":"posted-desc"},"pageNumber":"1"}}'
response = requests.post('https://www.squarefoot.com.hk/consumer/api/search', headers=headers, data=data)
print(response.text)