import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from time import sleep, time
import requests
from datetime import datetime
import json
import os
os.environ['NO_PROXY'] = 'stackoverflow.com'


# __testing = True
__testing = False
__testing_run = 2
__less_data = True
__less_data_run = 20  # for 50 pages

# historical_price == ricacorp


def init_firebase():
  # Use a service account
  if not len(firebase_admin._apps):
    cred = credentials.Certificate({
      "type": "service_account",
      "project_id": "findhome-217708",
      "private_key_id": "ab04be04d4040a9b3863d62ad26f8b75c67b302f",
      "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDSv+XtWnhJvyvZ\nnsLV6nQRfuMlwh5YFXCd+3yVze3+JldJTjO/FO5r4+LteSStXhX5szDvytSc27Go\nlemhOCIzJ7DqPoBM3QH58ti/l/T2DA4rE7qVz1YioQSPoQECbtOTojw9emmk2l2C\n6R9VujTdeEjbaZ7GwXm2iul846i5s/qHTpOT1HLd4htROvVWvG3d2JSx7G9D7DvB\nQnQIrr03lKXFHUoin1mHu0X4mJCc9EbMpvzc0eLlcVA3GIvSg0Zw66JI8oKUmfL3\ng+U/ZmI4MUoBRvXiAWQONifKZTY8lmykhlIQmNZEaOeurWqoJauLvpWxnpT7qFk6\nXGrWZHNVAgMBAAECggEAB4A/gi9sZRmPXryXwYPe/jQWiG9B47oy9YMwMmfE3v0a\nhw19aiwSoVdC1NdxOVTGMkqRBRPidd5xkrxv4eZXfJvJdZ45tE1QcZxAX9oGz2QV\n9AK3gHdY9jy7Zr5Lr3xmKOocZpwZsFL6WDtiZgt4MA1vhb8FtkbIGs8Tvjd0d6mH\nmKZLM56PyErxbCwmRDJnSjVx6P6bGHYIzbVLNPjFTHK1Tc5sASoLuZ4igKPX0w8A\nRlNSMAJIvM7tWovfuBI0O28UPWe6HM4UsAQIcEwunEFKElNz9CbHUv8rQwUTKKUn\n+ibBI2EmhkQG7UuKyPDQG9judFI9oQ4MyTPThv+sqQKBgQD103zOsJ+B0EqdN1GR\nlto/1byG+4frkRygG4VbRWKmggmQdwbYbhg8Pwu2ouvhzvbyHE2CdTsEihjuV2Ko\nmz1+PvwfUfSRSrOF8rekX0NrU5/QEmJs+PVjuiksqgaowBhCQQqMhWr4V4jbrTRB\nbLm1fn9BQ6Aj32hpcx28UEVXOQKBgQDbeMbt6nxDHbNA/p8yTlHepvJA3MaaNUVJ\nDXNRWKHbEqWRuQzoVcM96p5MOFe2j9qYV1BXDnd2WY2Jx6Z+Vawp0gkCYGtQr1n7\n2zlsMxEKJHsf7QY3LAqyM7+doGRQL3EHJxYvWIjedK9CpzLSM92eow0WFRyn5dcN\nQ5sxlYdA/QKBgEyhxxBvwumm0vu7mPP3wMWRjZnOo4ptzCylJ6dSM/SQKQcd88ZY\nW7m8NZyToVDdQeIrrIX0oZdCrY7vMtQ4wKywWcwxsjnwvZIOHe1hvRSiFvm4IXKz\nEVwrVlaUEktcxrMwr2cq2DPepc1FazrR9/p6GpVvzWYhaN0mg6iGpXcRAoGAVdqQ\nTx+sToD+z00Lr50tr6C1IaR72HVATkyBsO+wGUfZFwFByF1MTBtMo1pjDZqGk5OM\nbRHOr7BmiuBcBA21lIKCXbikImMGG1ztwKLVZpdQxvNsvAZxhLZzndDRjQCSAY9L\nwWer2wHgUX2ghtQdPOJhyyKlHBTwJFMvEZ2B1G0CgYAtXf9KcFAvSnge+3FFigp6\nyGnClB9TrG0qhmXZPLLYkKkDAACitSQXlZ4EQ5PW3Xr2EKlC/OgEueyPMm5nz+R/\n4CEf4AFGeX7efetfOq65y7ro7FIFbsP7eY7YUZOKFuifGfAWKvoCGmMkJR0nPqfo\ntH2JIdwkWcXBGPRuPTGoPg==\n-----END PRIVATE KEY-----\n",
      "client_email": "frank-00@findhome-217708.iam.gserviceaccount.com",
      "client_id": "106666234366259868875",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/frank-00%40findhome-217708.iam.gserviceaccount.com"
    })
    firebase_admin.initialize_app(cred)


def get_system_data__rentable():
  init_firebase()
  db = firestore.client()

  return db.collection('system_setting').document('_rentable').get().to_dict()['_rentable']


def add_data(collection_name, doc_name, data_in_dict):
  init_firebase()
  db = firestore.client()

  db.collection(collection_name).document(doc_name).set(data_in_dict)


def delete_data(collection_name, field_name, field_value):
  init_firebase()
  db = firestore.client()

  def del_limit(collection_name, field_name, field_value, batch_size=10):
    deleted = 0
    docs = db.collection(collection_name).where(field_name, u'==', field_value).limit(batch_size).stream()
    for doc in docs:
      print('--deleting:', doc.to_dict())
      doc.reference.delete()
      deleted = deleted + 1
    print('end del limit')
    if deleted >= batch_size:
      return del_limit(collection_name, field_name, field_value, batch_size)

  del_limit(collection_name, field_name, field_value, batch_size=10)


# def update_rent_me(collection_name, field_name, field_value):
#   init_firebase()
#   db = firestore.client()
#
#   def update_limit(collection_name, field_name, field_value, batch_size=10):
#     updated = 0
#     docs = db.collection(collection_name).where(field_name, u'==', field_value).limit(batch_size).stream()
#     for doc in docs:
#       print('--deleting:', doc.to_dict())
#       doc.update({'please_rent_me': False})
#       # doc.reference.delete()
#       doc.reference.update(doc)
#       updated = updated + 1
#     print('end del limit')
#     if updated >= batch_size:
#       return update_limit(collection_name, field_name, field_value, batch_size)
#
#   update_limit(collection_name, field_name, field_value, batch_size=10)


def set_data(collection_name, document_name, data_in_dict):
  # To create or overwrite a single document, use the set() method:
  init_firebase()
  db = firestore.client()
  db.collection(collection_name).document(document_name).set(data_in_dict)


def swich__rentable_and_update_data(___rentable):
  if ___rentable == 'a':
    set_data('system_setting', '_rentable2', {'_rentable': 'b'})
    # delete_data('historical_price', '_rentable', 'b')
    delete_data('squarefoot', '_rentable', 'b')
    # update_rent_me('historical_price', '_rentable_to_check', 'b')
    # update_rent_me('squarefoot', '_rentable_to_check', 'b')
  else:
    set_data('system_setting', '_rentable2', {'_rentable': 'a'})
    # delete_data('historical_price', '_rentable', 'a')
    delete_data('squarefoot', '_rentable', 'a')
    # update_rent_me('historical_price', '_rentable', 'a')
    # update_rent_me('squarefoot', '_rentable', 'a')


def ricacorp(___rentable, page=0):
  # src = https://property.ricacorp.com/post;type=r;language=EN;page=1;orderBy=overallDateModified%20desc
  for lang in ['EN', 'HK']:  # the language of the crawled data
    while True:
      if __testing:
        if page > __testing_run:
          return
      if __less_data:
        if page > __less_data_run:
          return
      page = page + 1
      url = 'https://property.ricacorp.com/rcAPI/rcPost?page={}&offset={}&' \
            'language={}&orderBy=overallDateModified%20desc&agreementType=5'.format(page, (page - 1) * 10, lang)
      req = requests.get(url).text
      d = json.loads(req)
      print('doing ricacorp, page =', page, 'lang=', lang)

      try:
        now_time = datetime.now()
        for doc in d['results']:
          doc.update({'_rentable_to_check': ___rentable})
          doc.update({'_source': 'ricacorp'})
          doc.update({'_rentable': True})
          doc.update({'_create': now_time})
          add_data('historical_price', doc['id'], doc)
      except IndexError:
        print('-- the end of ricacorp() --', lang)
        break
      sleep(0.5)



def squarefoot(___rentable, page=1):
  # https://www.squarefoot.com.hk/en/rent/list/?sortBy=posted-desc&page=1
  if __testing:
    if page > __testing_run:
      return
  if __less_data:
    if page > __less_data_run:
      return
  print('doing squarefoot, page = ', page)
  headers = {'content-type': 'application/json'}
  data = '{"search":{"channel":"rent","suggestions":[],"filters":{"sortBy":"posted-desc"},"pageNumber":"' + str(
    page) + '"}}'
  response = json.loads(requests.post('https://www.squarefoot.com.hk/consumer/api/search', headers=headers, data=data).text)
  # print(response['items'])  # <------------
  # if response["nextPageToken"]:
  #     return squarefoot(response["nextPageToken"])
  # else:
  #     return print('-- end of squarefoot()')
  now_time = datetime.now()
  if (response['items']):
    for doc in response['items']:
      doc.update({'_rentable_to_check': ___rentable})
      doc.update({'_source': 'squarefoot'})
      doc.update({'_rentable': True})
      doc.update({'_create': now_time})
      add_data('squarefoot', doc['id'], doc)
    sleep(0.5)
    return squarefoot(___rentable, page + 1)
  else:
    return


def midland_18_district():
  # source 'https://www.midland.com.hk/property-price-chart/'
  url = 'https://wws.midland.com.hk/mpp/data/district_pricechange?lang=en'
  req = requests.get(url).text
  d = json.loads(req)['district']
  for doc in d.values():
    doc.update({'_create': datetime.now()})
    print(doc)
    set_data('reference_price', doc['dist_id'], doc)


def get_system_data__rentable2():
  init_firebase()
  db = firestore.client()

  return db.collection('system_setting').document('_rentable2').get().to_dict()['_rentable']


def job(request):
  start = time()
  ___rentable = get_system_data__rentable2()
  print('___rentable:', ___rentable)
  squarefoot(___rentable)
  swich__rentable_and_update_data(___rentable)
  return str(time()-start)

# job(1)
