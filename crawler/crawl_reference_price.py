# import pandas as pd
# from bs4 import BeautifulSoup as bs
from requests import get
import json
from firebase_func import set_data


# def cppcl():
#     url = 'http://cppcl.property.hk/new/tran.php'
#     df = pd.read_html(url)
#     for d in df:
#         print(d.to_string())


def midland_18_district():
    # source 'https://www.midland.com.hk/property-price-chart/'
    url = 'https://wws.midland.com.hk/mpp/data/district_pricechange?lang=en'
    req = get(url).text
    d = json.loads(req)['district']
    for dd in d.values():
        print(dd)
        set_data('reference_price', dd['dist_id'], dd)


if __name__ == '__main__':
    midland_18_district()

    # set_data('reference_price', '01', {'di': 'q'})
